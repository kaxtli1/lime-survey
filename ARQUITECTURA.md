
# Arquitectura 


Este documento describe la arquitectura y las tecnologías utilizadas en el despliegue de LimeSurvey.

El servicio de Encuestas LimeSurvey está desplegado en los sistemas de alta disponibilidad implementados por la Subdirección de Infraestructura, diseñados para brindar alto desempeño y almacenamiento resiliente.



![](arquitectura.png)

Los componentes del diagrama se describen a continuación.

### LimeSurvey

Es una herramienta para la creación y levantamiento de encuestas en línea, la más popular de código abierto/software libre. 

Facilita la elaboración de encuestas a través de una WebUI de fácil manejo. Cuenta con una API REST que permite la extensión e integración con otras aplicaciones. También posee una interfaz de línea de comandos CLI, con funcionalidad limitada.

En este proyecto se utiliza el código distribuido directamente desde  la página oficial de LimeSurvey.

Se optó por la versión LTS de la serie 3.x por motivos de estabilidad. La versión instalada es la 3.27.28.

### Yii PHP Framework

Yii es un marco de trabajo escrito en PHP con un diseño modular y basado en eventos. Facilita la creación de aplicaciones PHP. Se puede utilizar la API de Yii para desarrollar extensiones o aplicaciones que interactúen con LimeSurvey.

### PHP

PHP es un popular lenguaje de programación para desarrollo de aplicaciones web del lado del servidor. Cuenta con una larga trayectoria en la que se han crea gran cantidad de recursos, entre librerías, aplicaciones y documentación. Dada su popularidad, el soporte que se puede encontrar es muy amplio.

Puede ejecutarse como demonio php-fpm o como módulo de Apache Web Server. En el primer caso, se puede lograr mayor flexibilidad, mientras que en segundo, al funcionar como módulo integrado en el servidor web, se puede obtener mejor desempeño en ciertos casos.

Se utiliza la distribución oficial de PHP que se encuentra en docker.io en su versión 7.4.

### Apache Web Server

El servidor web apache llegó a ser el servidor más utilizado en toda la Internet en la década de los 90s. Su larga trayectoria lo hacen un software con un alto grado de confiablilidad y establilidad. 

El contenedor utiliza la distribución de Apache 2.4.52 que se encuentra en Debian.

### Debian

Debian es una distribución reconocida por su estabilidad. Una gran mayoría de aplicaciones distribuyen sus versiones oficiales de contenedores, en imágenes basadas de Debian. Esto se debe a que el proceso de instalación que utiliza, está basado en debootstrap. Con esta herramienta es un trabajo trivial la creación de un contenedor relativamente pequeño con una versión oficial de Debian. 

La imagen oficial de PHP utilizada para crear el contenedor de LimeSurvey está basada en Debian.


### Docker / OCI

Las imágenes son creadas utilizando docker, sin embargo son compatibles con el estándar abierto OCI. 

El contenedor se compone de la siguiente pila:

- Debian
- Apache Web Server
- PHP
- Yii Framework
- LimeSurvey


### Kubernetes

Kubernetes es el orquestador de contenedores más popular. Su despliegue declarativo disminuye en gran medida la complejidad de la administración del cluster.

No gestiona los contenedores directamente, sino que crea contenedores de contenedores llamados pods. 

Kubernetes se encarga de mantener en operación las instancias de la imagen de LimeSurvey.

### HAProxy

HAProxy es un proxy inverso que realiza el balanceo de carga del tráfico TCP y HTTP.  Es el componente frontal que recibe todas las solicitudes entrantes desde Internet y las balancea en capa 7, pudiendo redireccionar según la ruta en la URL de ingreso.

El algoritmo utilizado para el balanceo es el Roundrobin, que divide la carga en partes iguales para cada contenedor.  No realiza ninguna diferenciación entre las solicitudes para la subida de archivos y los accesos php.

### Galera

Galera es un cluster para MySQL/MariaDB que opera en modo multi-maestro, evitando los problemas de la arquitectura maestro-trabajador.  Realiza la replicación de forma síncrona, por lo que la resiliencia a la perdida de datos es muy alta.

### GlusterFS

Gluster es un sistema de archivos implementado como una capa superior o gatewey de un sistema de almacenamiento de objetos. Tiene una arquitectura de capas bien definida que facilita el desarrollo de nuevos módulos. 

La idea central del almacenamiento de objetos consiste en eliminar el cuello de botella que genera la centralización de los metadatos. Los clientes pueden acceder directamente a los nodos que contienen los datos sin necesidad de consultar un servidor de metadatos. De esta forma el sistema se vuelve altamente escalable horizontalmente y muy eficiente.

## Cierre

En resumen, la implementación de LimeSurvey integra fácilmente a los servicios tecnológicos de alta disponibilidad administrados en la Coordinación de Infraestructura y se beneficia directamente de ellos para ofrecer un sistema eficiente y confiable.

