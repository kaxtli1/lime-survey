#!/bin/bash
# Conacyt
# 2020-03-19
# oscar.perez@comimsa.com
# Guión de inicialización para ejecutarse por el initContainer

# Descripción:
#
# 	El directorio  /var/www/app/init es agregado para
#	realizar las tareas de inicialización/instalación/instanciación
# 	de la aplicación en contenedores.

# Debe ser llamado usando la ruta completa
# command: /var/www/app/init/init.sh

# Las siguientes variables de entorno son obligatorias:
#
#       - LIMESURVER_ADMIN
#       - LIMESURVEY_ADMIN_PASSWORD
#       - LIMESURVEY_ADMIN_NAME
#       - LIMESURVEY_ADMIN_MAIL
#       - MARIADB_DATABASE=limesurvey
#       - MARIADB_USER=limesurvey
#       - MARIADB_PASSWORD
#	- PHP_HOST=limesurvey

cd $(dirname $0)

if [ -f init_date ] ; then
     echo "Ya inicializado previamiente... omitiendo inicialización"
     exit 0
fi

# Configura php-fpm para TCP

sed -i "s/^listen =.*/listen = 0.0.0.0:9000/" /etc/php/7.4/fpm/pool.d/www.conf 
sed -i "s/fastcgi_pass *unix:.*/fastcgi_pass $PHP_HOST:9000;/" /etc/nginx/conf.d/app.conf


# Configura limesurvey
cp   ../application/config/config-sample-mysql.php ../application/config/config.php

sed "s/host=localhost/host=$MARIADB_HOST/" -i ../application/config/config.php
sed "s/dbname=limesurvey/dbname=$MARIADB_DATABASE/" -i ../application/config/config.php
sed "s/'username' => 'root'/'username' => '$MARIADB_USER'/" -i ../application/config/config.php
sed "s/'password' => 'root'/'password' => '$MARIADB_PASSWORD'/" -i ../application/config/config.php


cd ../application/commands

php console.php install $LIMESURVER_ADMIN $LIMESURVEY_ADMIN_PASSWORD $LIMESURVEY_ADMIN_NAME  $LIMESURVEY_ADMIN_MAIL

cd $(dirname $0)
echo $(date) >> init_date

exit 0
