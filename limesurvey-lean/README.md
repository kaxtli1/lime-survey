# LimeSurvey
## Presentación
- El proyecto limesurvey tiene el enfoque de administración por el usuario y no el de administración automatizada necesaria para contenedores.


# Imagen de Contenedores Base 

Entre las consideraciones que deben cumplise para la imagen base:

- Minimizar el tamaño de la imagen.
- Evitar instalacion de paquetes inecesarios para la operación en productivo.
- Incrementar la seguridad
- Mantener el mayor acercamiento a la distribución oficial
- Implementación simple, fácil de entender
- Imagen base, para derivar las apps.
- Flexible para permitir todo tipo de derivados.
- Interfaz Estandar que pueda ser utilizada facilmente por nuevos integrantes de un equipo.
- No intrusiva. Las adecuaciones se realizan sin modificar la fuente.


# Arquitectura 

## Contenedores
Principios:

* **separación de intereses ( separation of concerns )** El instalador es de otra naturaleza que la operación.
* **acoplamiento débil**  Pueden intercambiarse servidores web.
* **reproducibilidad** 
* **Homogeneidad**
* **Estándares abiertos**
* **inmutabilidad** 

Respetando los principios anteriores las desciciones de diseño son las siguientes:

* **Un proceso por contenedor.**  Los tres procesos centrales del sistema son el servidor web ( nginx ), el servidor de base de datos ( mariadb ) y el servidor de aplicación ( php ). Para cada uno se construye un contenedor dónde son el proceso raíz.

* **Inicialización separada** El proceso de inicialización/instalación/instanciación de la aplicación se considera en concepto y en práctica de diferente naturaleza que la operación. Por este motivo se realiza en un contenedor aparte ( initContainer )

* **Configuración centralizada** Los parámetros y archivos de configuración se guardan en el contenedor de la aplicación (en este caso el contenedor PHP) y se comparten a los demás contenedores. Por ejemplo, la definición del esquema de la base de datos y de los parámetros del servidor web se guardan en el contenedor php. 

* **Instalación desatendida**. La configuración manual es suceptible a errores.

* **Imágenes compatibles OCI**. Las imágenes creadas son compatibles con OCI y con docker. Puede usarse podman o docker como herramienta.



### Construcción de la Imagen

Para automatizar la creación de la imagen, se creó una imagen auxiliar para la descarga. 
La aplicación una ves descargada es copiada a la imagen final, evitando de este modo que pueda contener paquetes innecesarios para la operación. Con esto se logra disminuir la carga de la imagen y minimizar un posible vector de vulnerabilidad.

La función de la imagen de descarga es descargar, verificar y descomprimir la imagen oficial de limesurvey.

Como medida de seguridad, las claves de integridad para la verificación del archivo descargado debe de escribirse manualmente en el archivo contenedor.

	FROM    docker.io/bitnami/minideb as downloader
	ENV     LIMESURVEY_ZIP limesurvey3.28.3+220315.zip
	
	ENV TERM=linux
	COPY    $LIMESURVEY_ZIP /
	
	RUN apt update  \
	 && apt install -y ca-certificates curl wget busybox \
	 && mkdir /var/www/ \
	 && cd /var/www/ \
	 && busybox unzip /$LIMESURVEY_ZIP | wc  \
	 && mv limesurvey/ app/
	# &&     wget https://download.limesurvey.org/latest-stable-release/limesurvey5.2.2+211115.zip  | busybox unzip
	  -

**NOTA** 
En la distribución oficial de LimeSurvey no se encontró de un histórico confiable para la reproducibilidad de imágenes.

Imagen limesurvey
	
	FROM docker.io/bitnami/minideb:bullseye as php-fpm
	
	ENV DEBIAN_FRONTEND=noninteractive
	RUN apt update \
	 && apt install -y php7.4-fpm \
	 && sed 's#^error_log =.*#error_log = /dev/stdout#' -i  /etc/php/7.4/fpm/php-fpm.conf
	
	
	FROM    php-fpm
	COPY    --from=downloader /var/www/ /var/www/
	
	
	ENV     TERM=linux
	
	RUN apt update  \
	 && apt install -y php-mbstring php-xml php-mysql php-gd php-zip php-ldap php-imap \
	 && cd /var/www/ \
	 && chown -R www-data:www-data app/tmp/ app/upload/ app/application/config/ \
	 && find /var/www/ -type d -exec chmod 0755 {} \; \
	 && find /var/www/ -type f -exec chmod 0644 {} \;
	 
	
	COPY    rootfs/ /
	CMD     [ "/usr/sbin/php-fpm7.4",  "-F" ]
	
	

La imagen utiliza una configuración genérica dónde la app se encuenta en el direcctorio /var/www/app/.

Archivo docker compose

- **initContainer** Realiza la tarea de instalar de forma desatentida limesurvey, utilizando los parámetros definidos en el docker-compose.yml.

- **sockets UNIX**. Los contenedares pueden comunicarse a través de sockets UNIX en el directorio /run/mysqld/ y /run/php/. Esto en el caso de que las instancias se ejecuten en la  misma máquina anfitrión o en un pod.


- **sockets TCP** Los contenedores pueden comunicarse a través de la red con los puertos 9000 para PHP y el 3306 para mysql. Esto es útil en el caso  que se realice la instanciación de contenedores de forma distribuida.


- Las siguientes variables son obligatorias para el initConainer:
	- LIMESURVER_ADMIN=admin
	- LIMESURVEY_ADMIN_PASSWORD=
	- LIMESURVEY_ADMIN_NAME="Administrador de Limesurvey"
	- LIMESURVEY_ADMIN_MAIL=admin@conacyt.mx
	- MARIADB_HOST=localhost
	- MARIADB_DATABASE=limesurvey
	- MARIADB_USER=limesurvey
	- MARIADB_PASSWORD=
	
- /var/www/app/init/init.sh es el guión de inicialización del initContainer
debe llamarse con la ruta completa.
No realiza verificaciones. Si falla se detiene el docker-compose sin 
enviar mensaje de error.

Archivo docker compose

	# LimeSurvey
	#	- Utiliza socket en /run para conectarse
	#		- el volumen run_mysqld comparte el socket entre db y php-limesurvey
	#		- el volumen run_php comparte el socket entre php-limesurevy y nginx
	#
	#	- Si se fija MARIADB_HOST=localhost, se utilizará el socket
	#
	#	- Si se fija MARIADB_HOST=db, se utilizará TCP
	#
	#	- Utiliza un initContainer para realizar la instalación desatendida
	#
	#	- A travéz de depens_on service_completed_successfully se asegura que los
	#		contenedores no se ejecuten hasta que se complete la inicialización
	
	
	
	version: "3.3"
	services:
	
	  db:
	    image:  docker.io/library/mariadb:10.6
	    environment:
	      - MARIADB_DATABASE=limesurvey
	      - MARIADB_USER=limesurvey
	      - MARIADB_PASSWORD=aoeunateuaoleuh
	      - MARIADB_ROOT_PASSWORD=123456
	    healthcheck:
	      test: "mysql --user=root --password=123456 -e 'show databases'"
	    volumes:
	      - mysql:/var/lib/mysql
	      #- run_mysqld:/run/mysqld
	
	  init:
	#    image: registry.conexo.mx:5000/kaxtli/limesurvey:alpha    
	    build: .
	    depends_on:
	      db:
	        condition: service_healthy
	    environment:
	       - LIMESURVER_ADMIN=admin
	       - LIMESURVEY_ADMIN_PASSWORD=870dhthnkbyih6706ch
	       - LIMESURVEY_ADMIN_NAME=Administrador # Acepta una sola palabra
	       - LIMESURVEY_ADMIN_MAIL=admin@conacyt.mx
	       - MARIADB_HOST=db
	       - MARIADB_DATABASE=limesurvey
	       - MARIADB_USER=limesurvey
	       - MARIADB_PASSWORD=aoeunateuaoleuh
	       - PHP_HOST=limesurvey
	    command: /var/www/app/init/init.sh
	    volumes:
	      - www:/var/www/
	      - conf_php:/etc/php/7.4/fpm/
	      - conf_nginx:/etc/nginx/conf.d/
	      #- run_mysqld:/run/mysqld
	 
	
	  limesurvey:
	#    image: registry.conexo.mx:5000/kaxtli/limesurvey:alpha
	    build: .  
	    depends_on: 
	      init:
	        condition: service_completed_successfully
	    volumes:
	      - www:/var/www/
	      - conf_nginx:/etc/nginx/conf.d/
	      - conf_php:/etc/php/7.4/fpm/
	      #- run_mysqld:/run/mysqld
	      - run_php:/run/php # TAREA: desactivar socket en la configuración de php
	     
	  nginx:
	#    image: registry.conexo.mx:5000/kaxtli/nginx
	    build: nginx
	    depends_on:
	      - limesurvey
	    ports:
	      - 8080:80
	    volumes:
	      - www:/var/www/
	      - conf_nginx:/etc/nginx/conf.d/
	      #- run_php:/run/php
	    
	
	volumes:
	     #run_mysqld:
	     run_php:
	     conf_php:
	     conf_nginx:
	     www:
	     mysql:
	
	
Archivo de instalación

	#!/bin/bash
	# Conacyt
	# 2020-03-19
	# oscar.perez@comimsa.com
	# Guión de inicialización para ejecutarse por el initContainer
	
	# Descripción:
	#
	# 	El directorio  /var/www/app/init es agregado para
	#	realizar las tareas de inicialización/instalación/instanciación
	# 	de la aplicación en contenedores.
	
	# Debe ser llamado usando la ruta completa
	# command: /var/www/app/init/init.sh
	
	# Las siguientes variables de entorno son obligatorias:
	#
	#       - LIMESURVER_ADMIN
	#       - LIMESURVEY_ADMIN_PASSWORD
	#       - LIMESURVEY_ADMIN_NAME
	#       - LIMESURVEY_ADMIN_MAIL
	#       - MARIADB_DATABASE=limesurvey
	#       - MARIADB_USER=limesurvey
	#       - MARIADB_PASSWORD
	#	- PHP_HOST=limesurvey
	
	cd $(dirname $0)
	
	if [ -f init_date ] ; then
	     echo "Ya inicializado previamiente... omitiendo inicialización"
	     exit 0
	fi
	
	# Configura php-fpm para TCP
	
	sed -i "s/^listen =.*/listen = 0.0.0.0:9000/" /etc/php/7.4/fpm/pool.d/www.conf 
	sed -i "s/fastcgi_pass *unix:.*/fastcgi_pass $PHP_HOST:9000;/" /etc/nginx/conf.d/app.conf
	
	
	# Configura limesurvey
	cp   ../application/config/config-sample-mysql.php ../application/config/config.php
	
	sed "s/host=localhost/host=$MARIADB_HOST/" -i ../application/config/config.php
	sed "s/dbname=limesurvey/dbname=$MARIADB_DATABASE/" -i ../application/config/config.php
	sed "s/'username' => 'root'/'username' => '$MARIADB_USER'/" -i ../application/config/config.php
	sed "s/'password' => 'root'/'password' => '$MARIADB_PASSWORD'/" -i ../application/config/config.php
	
	
	cd ../application/commands
	
	php console.php install $LIMESURVER_ADMIN $LIMESURVEY_ADMIN_PASSWORD $LIMESURVEY_ADMIN_NAME  $LIMESURVEY_ADMIN_MAIL
	
	cd $(dirname $0)
	echo $(date) >> init_date
	
	exit 0
	
# Conclusiones
