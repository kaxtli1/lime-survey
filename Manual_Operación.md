# Manual del Operador 

## Importar encuestas

El proceso de crear encuesta se inicia desde la página de inicio de la aplicación de LimeSurvey.

![](img/encuestas-01.png)

La segunda pestaña corresponde a la Importación de encuestas

![](img/encuesta-02.png)

Se debe proporcionar el archivo **lsa** que contiene los datos de la encuesta.

![](img/encuestas-03.png)

Si se suben correctamente, debe mostrar un cuadro con un sumario del contenido del archivo. De lo contrarío, es probable que el archivo corresponda a otra versión de LimeSurvey. Los archivos de la versión 5 no son compatibles con la versión  3.


![](img/encuestas-04.png)



## Participantes
### Subir participantes

Para asignar a una encuesta los participantes, es necesario contar con un archivo **CSV** con los datos de los participantes.

El archivo debe ser CSV (delimitado por comas) estándar, con comillas dobles alrededor de cada registro (por defecto en OpenOffice y Excel). La primera línea debe contener los nombres de los campos, sin importar su orden.
Campos obligatorios: firstname, lastname, email
Campos opcionales: emailstatus, token, language, validfrom, validuntil, attribute_1, attribute_2, attribute_3, usesleft, ... . 

El primer paso es ingresar a la encuesta a través de la lista de encuestas.

![](img/participantes-01.png)

En el listado de encuestas, hacer clic a la encuesta subida en el apartado anterior.

![](img/participantes-02.png)

En el menú superior, selección el botón "Participantes de encuesta"

![](img/participantes-03.png)

Pedirá inicializar la tabla de participantes. No es necesario proporcionar ningún otro dato, simplemente se pulsa el botón **Incializar la tabla de participantes** y LimeSurvey creara una tabla en la base de datos.

![](img/participantes-04.png)

Responde con una confirmación de que realizó la construcción con éxito.

![](img/participantes-05.png)

Procedemos a poblar la tabla subiendo el archivo **CSV**. 

Para ello en el menú superior, se pulsa sobre el botón **crear** y la opción **Archivo CSV**

![](img/participantes-06.png)

Usando el botón **examinar** se selecciona el archivo **CSV** con los participantes.  Se respetan las opciones por omisión y se pulsa el botón **Subir**


![](img/participantes-07.png)

Si la carga del archivo se realiza correctamente desplegará un mensaje con el resumen de los datos procesados.

![](img/participantes-08.png)






### Configuración de Participantes

Una vez capturada la lista de participantes, se pueden ajustar preferencias en la forma en la que se realiza la participación. Para ello en el menú lateral de la encuesta, en el tabulador **Ajustes** se abre la opción **Configuración de participantes**

![](img/participantes-09.png)

Se muestra la siguiente pantalla.

![](img/participantes-10.png)

- **Habilitar persistencia basada en token** Si se habilita, los participantes que recibieron un token pueden interrumpir la encuesta y continuarla desde dónde la interrumpieron después sin perder datos. Solo necesitan usar el mismo token.
	
- **Permitir múltiples respuestas o actualización con token**. Si se habilita, los participantes que recibieron un token pueden usarlo para acceder **desde el principio** de la encuesta aún cuando la hayan terminado. Si **habilitar persistencia** está activo, podrán cambiar sus respuestas anteriores. Si **habilitar persistencia** está desactivado, se registrará un conjunto nuevo de respuestas. Es decir, responderán múltiples veces la encuesta.

- **Permitir registro público**. Los participantes pueden registrarse por ellos mismos. 
- 
- **Enviar confirmación por correo**. Al momento de abrir la encuesta, se enviará un correo a los participantes registrados que ya está disponible para su llenado.


Para guardar los cambios se debe pulsar el botón **Guardar**

![](img/guardar.png)



## Notificaciones
### Configurar cliente de correo

El servidor LimeSurvey debe estar configurado como cliente de correo SMTP para poder enviar notificaciones. La configuración necesaria se accede dese el menú **Configuración General**

![](img/notificaciones-05.png)

Ahí se deben capturar las credenciales de la cuenta de correo y los parámetros del servidor.

Servidor SMTP: **smtp.office365.com**

Encripción SMTP: **STARTTLS**


![](img/notificaciones-06.png)

Para que los cambios surtan efecto se debe pulsar el botón **Guardar**

![](img/guardar.png)

### Configuración de Notificaciones por Correo

Algunos parámetros de las notificaciones de corro se acceden desde el panel lateral, en la sección **Notificaciones y datos**

![](img/notificaciones-01.png)

La página contiene las siguientes opciones:



![](img/notificaciones-02.png)


De las cuales se pueden ajustar:

- **El participante puede salvar y continuar mas tarde** El participante tendrá la opción de pulsar un botón que le permite guardar su avance y continuar con la encuesta en otra ocasión.

- **Mandar correo de notificación básica de administrador a** Escribe el correo al que se desee que se envíen notificaciones de eventos de LimeSurvey.

- **Mandar correo de notificación detallada de administrador a** Escribe el correo al que se desee que se envíen notificaciones de eventos de LimeSurvey en formato extenso.


Recuerda que para conservar los cambios se debe pulsar el botón **Guardar**

![](img/guardar.png)

### Configurar Plantillas de Correo

Los mensajes que recibirán los participantes se pueden editar en las plantillas de correo.. Para acceder a su configuración accedemos a través del menú del panel izquierdo de la encuesta:

![](img/notificaciones-03.png)

Las plantillas para los participantes son:

- **Invitación**
- **Recordatorio**
- **Confirmación**
- **Registro**

Las plantillas para notificaciones del sistema:

- **Notificación básica de administrador**
- **Notificación detallada de administrador**


![](img/notificaciones-04.png)


**nota** El [Listado de etiquetas](https://manual.limesurvey.org/Email_templates ) completo se puede consultar en la página oficial de documentación de LimeSurvey.



**IMPORTANTE** La etiqueta  {OPTOUTURL} es reemplazada por una URL que permite a los destinatarios darse de baja de la lista de participantes. Si esto no es lo que se desea, debe eliminarse de la plantilla de correo.


Recuerda guardar tus cambios.

![](img/guardar.png)


### Enviar Notificaciones de correo


![](img/notificaciones-07.png)

![](img/notificaciones-08.png)

![](img/notificaciones-09.png)


Los dos botones del formulario están mal traducidos.

**Excluir los token dónde el  correos electrónico es incorrecto** (*token de bypass con direcciones de correo electrónico en su defecto:*) Solo envía correos a los participantes que su correo tiene status de correcto.


**Omitir el control de fecha antes de enviar correo** (*Control de fecha antes de enviar correo electrónico*) No toma en cuenta el rango de fechas en que es válido un token.


El envío de recordatorios puede ser programado:

- Días mínimos entre recordatorios.
- Recordatorios máximos.

![](img/notificaciones-10.png)






## Publicar Encuesta

La publicación de la encuesta la hace disponible a los participantes para su llenado. Previo a la publicación se pueden ajustar parámetros de su publicación. 

El menú se encuentra en el panel izquierdo de la encuesta

![](img/publicar-01.png)

Se puede asignar fecha de apertura y de cierre.

- Listar la encuesta públicamente. Aparecerá en el listado de encuestas de la portada principal del sitio.
- Conjunto de cookies para prevenir la participación repetida: Guarda en la máquina de usuario "cookies" para identificar una segunda visita.


![](img/publicar-02.png)


Previo a la publicación de la encuesta, se puede previsualizar desde el menú superior de la encuesta.

![](img/publicar-03.png)

Para activar la encuesta y que se encuentre disponible para los participantes, accedemos al botón **Activar encuesta** del menú superior de la encuesta.

![](img/publicar-04.png)

Pregunta por opciones de rastreo del usuario, como su dirección IP, la fecha, hora y URL con la que accedió al sitio. 

**IMPORTANTE** Una vez abierta la encuesta no se pueden modificar las preguntas.


![](img/publicar-05.png)

Si se activa con éxito la encuesta, envía el siguiente mensaje:


![](img/publicar-06.png)






## Cerrar encuesta

La tarea final del ciclo de vida de una encuesta es su cierre y extracción de resultados. Desde el menú superior de la encuesta podemos cerrarla.

![](img/publicar-07.png)

La forma **normal** de cerrar una encuesta es **Expirar**, es decir, llegó a su termino natural. Alternativamente, se puede cerrar una encuesta antes de su plazo programado, esto es **desactivar encuesta** 

**IMPORTANTE** **Desactivar una encuesta implica perder los datos recolectados hasta el momento.**

![](img/publicar-08.png)


Una vez cerrada, se puede acceder a las respuestas desde el menú lateral izquierdo de la encuesta:

![](img/publicar-10.png)

Una vez en esa página, se pueden  exportar las respuestas desde el menú superior.

![](img/publicar-11.png)

Antes de exportar se puede restringir el conjunto de datos con varios parámetros. Se recomienda utilizar las opciones por omisión.



![](img/publicar-12.png)

Se puede acceder a los gráficos estadísticos desde el menú lateral de la encuesta:

![](img/publicar-13.png)

En la parte superior se activa el modo simple para visualizarlos con las opciones más comunes:

![](img/publicar-14.png)

Se muestra un gráfico por pregunta

![](img/publicar-15.png)

Los gráficos se pueden exportar en un archivo zip, pulsando el botón **Exportar imágenes**


![](img/publicar-16.png)



