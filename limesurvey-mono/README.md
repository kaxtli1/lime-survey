# LimeSurvey Mono v3



## Presentación

Esta es implementación reailazda en la Subdirección de Infraestructura de la imagen OCI y la documentación. Se realizaron pequeños ajustes:

v4
- 

v3
- Realiza instalación desatendida mediante un initContainer

v2:
- Se redujo la cantidad de capas en la imagen.
- Se sustituye el uso de link que será obsoleto por depends_on
- Se utilizan rutas absolutas en las referencias de las imágenes



## Uso 

Para ejecutar limesurvey localmente se tiene el siguiente procedimiento:

### 0. Descargar los archivos de configuración

De este repositorio, descarga la carpeta limesurvey-mono que corresponde a la variante de LimeSurvey en un solo contenedor y que es la que se describe en este documento. Todos los siguientes pasos deben realizarse dentro de la carpeta.

	cd limesurvey-mono
	
Alternativamente se pueden crear los archivos necesarios de forma manual con el contenido que se encuentra en la última sección de este documento.

### 1. Descargar el archivo de la distribución oficial de LimeSurvey

El proyecto LimeSurvey no mantiene un repositorio histórico en línea desde dónde se puedan descargar las versiones anteriores a la estable. Por lo que se debe ajustar el nombre de la imagen según  corresponda a la publicada en la fecha de la descarga.

La  elegida es de la rama LTS 3.x. En la fecha de instalación se descargó la versión 3.27.28.


	wget https://download.limesurvey.org/lts-releases/limesurvey3.27.31+220104.zip 
	unzip limesurvey*.zip
	mv limesurvey*/ limesurvey/


### 2. Crear la imagen del contenedor

A partir de la versión 4 ya no es necesario construir el contenedor antes de lanzan docker-compose.

La imagen OCI del contenedor se crea 

	docker build -t limesurvey-mono .

### 3. Lanzar el servicio

El servicio se levanta utilizando docker-compose. 

	docker-compose up -d

Construye la imagen del contenedor si no está construida y ealiza la instalación desatendida.


## Archivos

El procedimiento anterior puede realizase creando directamente el contenido de los siguientes archivos:

La imagen OCI se crea con el siguiente archivo Dockerfile/Containerfile:


	FROM docker.io/php:7.4-apache
		# la imagen de docker.io usa el apache de debian
	
	COPY ./limesurvey /var/www/html/
	
	RUN apt update \
	 && apt install -y libpq-dev libldap2-dev libzip-dev zip libc-client-dev \
	    libkrb5-dev libssl-dev libfreetype6-dev libjpeg62-turbo-dev libpng-dev \
	 && docker-php-ext-configure pgsql -with-pgsql=/usr/local/pgsql \
	 && docker-php-ext-install pdo pdo_pgsql pgsql \
	 && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu/ \
	 && docker-php-ext-install ldap \
	 && docker-php-ext-install zip \
	 && docker-php-ext-configure imap --with-kerberos --with-imap-ssl \
	 && docker-php-ext-install  imap \
	 && docker-php-ext-configure gd --with-freetype --with-jpeg \
	 && docker-php-ext-install  gd \
	 && docker-php-ext-install mysqli pdo_mysql \
	 && a2enmod rewrite \
	 && chown -R www-data:www-data /var/www/html
	
	COPY ./rootfs/ /
	
	CMD ["apache2-foreground"]
		

El archivo para **docker-compose.yml**  es el siguiente:

	version: '2'
	
	services:
	  mysql:
	    image: container-registry.oracle.com/mysql/community-server
	    volumes:
	      - mysql:/var/lib/mysql
	    restart: always
	    environment:
	        MYSQL_ROOT_PASSWORD: limesurvey
	        MYSQL_DATABASE: limesurvey
	        MYSQL_USER: limesurvey
	        MYSQL_PASSWORD: 1234
	    healthcheck:
	      test: "mysql --user=root --password=limesurvey -e 'show databases'"
	
	  limesurvey:
	    image:
	      limesurvey-mono
	    depends_on:
	       - mysql      
	    ports:
	      - "80:80"
	    volumes:
	      - upload:/app/upload
	      - config:/var/www/html/application/config/
	
	  init:
	    image:
	      limesurvey-mono
	    depends_on:
	      mysql:
	        condition: service_healthy
	    environment:
	       - LIMESURVER_ADMIN=admin
	       - LIMESURVEY_ADMIN_PASSWORD=870dhthnkbyih6706ch
	       - LIMESURVEY_ADMIN_NAME=Administrador # Acepta una sola palabra
	       - LIMESURVEY_ADMIN_MAIL=admin@conacyt.mx
	       - MARIADB_HOST=mysql
	       - MARIADB_DATABASE=limesurvey
	       - MARIADB_USER=limesurvey
	       - MARIADB_PASSWORD=1234
	       - PHP_HOST=limesurvey
	    command: /var/www/html/init/init.sh
	    volumes:
	      - config:/var/www/html/application/config/
	 
	
	volumes:
	  mysql:
	  upload:
	  config:
	

El instalador/iniciador/instanciador del initContainer es:

	cd $(dirname $0)
	
	if [ -f init_date ] ; then
	     echo "Ya inicializado previamiente... omitiendo inicialización"
	     exit 0
	fi
	
	
	# Configura limesurvey
	cp   ../application/config/config-sample-mysql.php ../application/config/config.php
	
	sed "s/host=localhost/host=$MARIADB_HOST/" -i ../application/config/config.php
	sed "s/dbname=limesurvey/dbname=$MARIADB_DATABASE/" -i ../application/config/config.php
	sed "s/'username' => 'root'/'username' => '$MARIADB_USER'/" -i ../application/config/config.php
	sed "s/'password' => 'root'/'password' => '$MARIADB_PASSWORD'/" -i ../application/config/config.php
	
	
	cd ../application/commands
	
	php console.php install $LIMESURVER_ADMIN $LIMESURVEY_ADMIN_PASSWORD $LIMESURVEY_ADMIN_NAME  $LIMESURVEY_ADMIN_MAIL
	
	cd $(dirname $0)
	echo $(date) >> init_date
	
	exit 0
	

Para que funcione es obligatorio definir las variables de entorno:

       - LIMESURVER_ADMIN
       - LIMESURVEY_ADMIN_PASSWORD
       - LIMESURVEY_ADMIN_NAME
       - LIMESURVEY_ADMIN_MAIL
       - MARIADB_DATABASE=limesurvey
       - MARIADB_USER=limesurvey
       - MARIADB_PASSWORD
	   - PHP_HOST=limesurvey
	
